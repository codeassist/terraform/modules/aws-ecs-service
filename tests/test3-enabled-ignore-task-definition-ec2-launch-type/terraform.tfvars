# --------------------------
# Common/General parameters
# --------------------------
# Whether to create the resources ("false" prevents the module from creating any resources).
ecs_service_module_enabled = true
# Emulation of `depends_on` behavior for the module (Non zero length string can be used to have current module
# wait for the specified resource).
ecs_service_module_depends_on = null
# (Required) The name of the service (up to 255 letters, numbers, hyphens, and underscores).
ecs_service_name = "test3"
# ----------------------------
# Network/Security parameters
# ----------------------------
# (Required) The VPC ID where resources are created.
ecs_service_vpc_id = "vpc-test3"
# To prevent an error `count depends` or `count can not be computed`.
ecs_service_allowed_security_groups_count = 0
# List of the Security Groups IDs aloowed to connect to the service.
ecs_service_allowed_security_groups_list = []
# To prevent an error `count depends` or `count can not be computed`.
ecs_service_allowed_cidr_blocks_count = 0
# List of the CIDR block allowed to connect to the service.
ecs_service_allowed_cidr_blocks = []
# List of the TCP ports will be open on the ECS service.
ecs_service_open_ports_list = []
# -----------------------
# ECS Service parameters
# -----------------------
# Parameter in the ECS service definition whose changes will be ignored. Allowed values are:
#   * desired_count
#   * task_definition
ecs_service_ignore_parameter_changes = ""
# (Required) The family and revision (family:revision) or full ARN of the task definition that you want to run
# in your service.
ecs_service_task_definition_arn = "arn:aws:::ecs-task/test3"
# The capacity provider strategy to use for the service. Can be one or more.
ecs_service_capacity_provider_strategy = []
# The network mode that task used.
ecs_service_task_network_mode = ""
# The number of instances of the task definition to place and keep running.
ecs_service_desired_count = 1
# (Optional) The launch type on which to run your service. The valid values are:
#   * EC2
#   * FARGATE
ecs_service_launch_type = "FARGATE"
# (Required) The ARN of the ECS cluster where service will be provisioned.
ecs_service_cluster_arn = "arn:aws:::ecs-cluster/test3"
# Type of deployment controller. Valid values are `CODE_DEPLOY` and `ECS`.
ecs_service_deployment_controller_type = "ECS"
# The upper limit of the number of tasks (as a percentage of `desired_count`) that can be running in a service
# during a deployment.
ecs_service_deployment_maximum_percent = 200
# The lower limit (as a percentage of `desired_count`) of the number of tasks that must remain running and
# healthy in a service during a deployment.
ecs_service_deployment_minimum_healthy_percent = 100
# Specifies whether to propagate the tags from the task definition or the service to the tasks. The valid
# values are SERVICE and TASK_DEFINITION.
ecs_service_propagate_tags = null
# (Optional) Service level strategy rules that are taken into consideration during task placement. List from top
# to bottom in order of precedence. The maximum number of `ordered_placement_strategy` blocks is "5".
ecs_service_ordered_placement_strategy = []
# Seconds to ignore failing load balancer health checks on newly instantiated tasks to prevent premature
# shutdown, up to 7200. Only valid for services configured to use load balancers.
ecs_service_health_check_grace_period_seconds = 0
# A list of load balancer config objects for the ECS service. See `load_balancer` docs:
#   * https://www.terraform.io/docs/providers/aws/r/ecs_service.html
# NOTE: multiple `load_balancer` configuration block support was added in Terraform AWS Provider version 2.22.0.
#       This allows configuration of ECS service support for multiple target groups.
ecs_service_load_balancers = []
# Rules that are taken into consideration during task placement. Maximum number of `placement_constraints` is 10.
ecs_service_placement_constraints = []
# (Required) The subnets associated with the task or service.
ecs_service_subnet_ids = [
  "subnet-test3"
]
# Security group IDs to allow in Service `network_configuration`.
ecs_service_security_group_ids = []
# Assign a public IP address to the ENI (Fargate launch type only). Valid values are `true` or `false`.
ecs_service_assign_public_ip = false
# The service discovery registries for the service (the maximum number of service_registries blocks is 1).
ecs_service_discovery_service_registries = []
