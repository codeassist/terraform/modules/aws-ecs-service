terraform {
  required_version = "~> 0.12.0"

  required_providers {
    # NOTE(!): multiple `load_balancer` configuration block support was added in Terraform AWS Provider version 2.22.0.
    #          This allows configuration of ECS service support for multiple target groups.
    # NOTE(!!): capacity_service_provider config block support was added in Terraform AWS Provider starting from
    #           ver. 2.42.0
    aws = ">= 2.70"
  }
}
