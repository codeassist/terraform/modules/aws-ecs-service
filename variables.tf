# --------------------------
# Common/General parameters
# --------------------------
variable "ecs_service_module_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # "false" prevents the module from creating any resources
  default = false
}
variable "ecs_service_module_depends_on" {
  description = "Emulation of `depends_on` behavior for the module."
  type        = any
  # Non zero length string can be used to have current module wait for the specified resource.
  default = null
}
variable "ecs_service_name" {
  description = "(Required) The name of the service (up to 255 letters, numbers, hyphens, and underscores)."
  type        = string
}

# ----------------------------
# Network/Security parameters
# ----------------------------
variable "ecs_service_vpc_id" {
  description = "(Required) The VPC ID where resources are created."
  type        = string
}
variable "ecs_service_allowed_security_groups_count" {
  description = "To prevent an error `count depends` or `count can not be computed`."
  type        = number
  default     = 0
}
variable "ecs_service_allowed_security_groups_list" {
  description = "List of the Security Groups IDs allowed to connect to the service."
  type        = list(string)
  default     = []
}
variable "ecs_service_allowed_cidr_blocks_count" {
  description = "To prevent an error `count depends` or `count can not be computed`."
  type        = number
  default     = 0
}
variable "ecs_service_allowed_cidr_blocks" {
  description = "List of the CIDR block allowed to connect to the service."
  type        = list(string)
  default     = []
}
variable "ecs_service_open_ports_list" {
  description = "List of the TCP ports will be open on the ECS service."
  type        = list(number)
  default     = []
}

# -----------------------
# ECS Service parameters
# -----------------------
variable "ecs_service_ignore_parameter_changes" {
  description = "Parameter in the ECS service definition whose changes will be ignored."
  type        = string
  # Allowed values are:
  #   * desired_count
  #   * task_definition
  default = ""
}
variable "ecs_service_task_definition_arn" {
  description = "(Required) The family and revision (family:revision) or full ARN of the task definition that you want to run in your service."
  type        = string
}
variable "ecs_service_cluster_arn" {
  description = "(Required) The ARN of the ECS cluster where service will be provisioned."
  type        = string
}
variable "ecs_service_capacity_provider_strategy" {
  description = "The capacity provider strategy to use for the service. Can be one or more."
  type = list(object({
    # (Required) The short name or full Amazon Resource Name (ARN) of the capacity provider.
    capacity_provider = string
    # (Required) The relative percentage of the total number of launched tasks that should use the specified capacity
    # provider.
    weight = number
    # (Optional) The number of tasks, at a minimum, to run on the specified capacity provider. Only one capacity
    # provider in a capacity provider strategy can have a base defined.
    base = number
  }))
  default = []
}

variable "ecs_service_deployment_controller_type" {
  description = "Type of deployment controller. Valid values are `CODE_DEPLOY` and `ECS`."
  type        = string
  default     = "ECS"
}
# 200/100   - means ECS Service first spins up one more Task, waits until it becomes healthy and only then,
#             after undefined pause, will turn off the previous Task instance.
# 100/0     - means ECS Service first turns off the previous Task instance and only then will try to spin up a new one.
variable "ecs_service_deployment_minimum_healthy_percent" {
  description = "The lower limit (as a percentage of `desired_count`) of the number of tasks that must remain running and healthy in a service during a deployment."
  type        = number
  default     = 100
}
variable "ecs_service_deployment_maximum_percent" {
  description = "The upper limit of the number of tasks (as a percentage of `desired_count`) that can be running in a service during a deployment."
  type        = number
  default     = 200
}
variable "ecs_service_desired_count" {
  description = "The number of instances of the task definition to place and keep running."
  type        = number
  default     = 1
}
variable "ecs_service_health_check_grace_period_seconds" {
  description = "Seconds to ignore failing load balancer health checks on newly instantiated tasks to prevent premature shutdown, up to 7200. Only valid for services configured to use load balancers."
  type        = number
  default     = 0
}
variable "ecs_service_launch_type" {
  description = "(Optional) The launch type on which to run your service."
  # The valid values are:
  #   * EC2
  #   * FARGATE
  type    = string
  default = "EC2"
}
variable "ecs_service_load_balancers" {
  description = "A list of load balancer config objects for the ECS service."
  # See `load_balancer` docs:
  #   * https://www.terraform.io/docs/providers/aws/r/ecs_service.html
  # NOTE: multiple `load_balancer` configuration block support was added in Terraform AWS Provider version 2.22.0.
  #       This allows configuration of ECS service support for multiple target groups.
  type = list(object({
    # (Required for ELB Classic) The name of the ELB (Classic) to associate with the service.
    elb_name = string
    # (Required for ALB/NLB) The ARN of the Load Balancer target group to associate with the service.
    target_group_arn = string
    # (Required) The name of the container to associate with the load balancer (as it appears in a container
    # definition).
    container_name = string
    # (Required) The port on the container to associate with the load balancer.
    container_port = number
  }))
  default = []
}
### for `network_configuration` block ###
variable "ecs_service_task_network_mode" {
  description = "The network mode that task used."
  type        = string
  default     = ""
}
variable "ecs_service_subnet_ids" {
  description = "(Required) The subnets associated with the task or service."
  type        = list(string)
}
variable "ecs_service_security_group_ids" {
  description = "Security group IDs to allow in Service `network_configuration`."
  type        = list(string)
  default     = []
}
variable "ecs_service_assign_public_ip" {
  description = "Assign a public IP address to the ENI (Fargate launch type only). Valid values are `true` or `false`. Default `false`."
  type        = bool
  default     = false
}
### ---
variable "ecs_service_ordered_placement_strategy" {
  description = "(Optional) Service level strategy rules that are taken into consideration during task placement."
  # List from top to bottom in order of precedence.
  # The maximum number of `ordered_placement_strategy` blocks is "5".
  type = list(object({
    # (Required) The type of placement strategy. Must be one of:
    #   * binpack
    #   * random
    #   * spread
    type = string
    # (Optional) For the spread placement strategy, valid values are "instanceId" (or "host", which has the same
    # effect), or any platform or custom attribute that is applied to a container instance.
    # For the "binpack" type, valid values are "memory" and "cpu".
    # For the "random" type, this attribute is not needed.
    # For more information, see Placement Strategy:
    #   * https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_PlacementStrategy.html
    field = string
  }))
  default = []
}
variable "ecs_service_placement_constraints" {
  description = "Rules that are taken into consideration during task placement. Maximum number of `placement_constraints` is 10."
  type = list(object({
    # (Required) The type of constraint. The only valid values at this time are "memberOf" and "distinctInstance".
    type = string
    # (Optional) Cluster Query Language expression to apply to the constraint. Does not need to be specified for
    # the "distinctInstance" type. For more information, see:
    #   * https://docs.aws.amazon.com/AmazonECS/latest/developerguide/cluster-query-language.html
    expression = string
  }))
  default = []
}
variable "ecs_service_propagate_tags" {
  description = "Specifies whether to propagate the tags from the task definition or the service to the tasks. The valid values are SERVICE and TASK_DEFINITION."
  type        = string
  default     = null
}
variable "ecs_service_discovery_service_registries" {
  description = "The service discovery registries for the service."
  # The maximum number of service_registries blocks is 1.
  type = list(object({
    # (Required) The ARN of the Service Registry. The currently supported service registry is Amazon Route 53 Auto
    # Naming Service(`aws_service_discovery_service`). For more information, see:
    #   * https://docs.aws.amazon.com/Route53/latest/APIReference/API_autonaming_Service.html
    registry_arn = string
    # (Optional) The port value used if your Service Discovery service specified an SRV record.
    port = number
    # (Optional) The port value, already specified in the task definition, to be used for your service discovery
    # service.
    container_port = number
    # (Optional) The container name value, already specified in the task definition, to be used for your service
    # discovery service.
    container_name = string
  }))
  default = []
}
