output "service_id" {
  description = "The Amazon Resource Name (ARN) that identifies the service."
  value       = local.enabled ? (lower(var.ecs_service_ignore_parameter_changes) == "desired_count" ? join("", aws_ecs_service.ignored_desired_count.*.id) : (lower(var.ecs_service_ignore_parameter_changes) == "task_definition" ? join("", aws_ecs_service.ignored_task_definition.*.id) : join("", aws_ecs_service.default.*.id))) : ""
}

output "service_name" {
  description = "The name of the ECS Service."
  value       = local.enabled ? (lower(var.ecs_service_ignore_parameter_changes) == "desired_count" ? join("", aws_ecs_service.ignored_desired_count[*].name) : (lower(var.ecs_service_ignore_parameter_changes) == "task_definition" ? join("", aws_ecs_service.ignored_task_definition[*].name) : join("", aws_ecs_service.default[*].name))) : ""
}

output "service_cluster" {
  description = "The Amazon Resource Name (ARN) of cluster which the service runs on."
  value       = local.enabled ? (lower(var.ecs_service_ignore_parameter_changes) == "desired_count" ? join("", aws_ecs_service.ignored_desired_count[*].cluster) : (lower(var.ecs_service_ignore_parameter_changes) == "task_definition" ? join("", aws_ecs_service.ignored_task_definition[*].cluster) : join("", aws_ecs_service.default[*].cluster))) : ""
}

output "service_desired_count" {
  description = "The number of instances of the task definition."
  value       = local.enabled ? (lower(var.ecs_service_ignore_parameter_changes) == "desired_count" ? join("", aws_ecs_service.ignored_desired_count[*].desired_count) : (lower(var.ecs_service_ignore_parameter_changes) == "task_definition" ? join("", aws_ecs_service.ignored_task_definition[*].desired_count) : join("", aws_ecs_service.default[*].desired_count))) : ""
}

output "service_role_arn" {
  description = "The ARN of the IAM role used by ECS Service."
  value       = join("", aws_iam_role.ecs_service.*.arn)
}

output "service_security_group_id" {
  description = "The Security Group ID of the ECS service."
  value       = join("", aws_security_group.ecs_service.*.id)
}
