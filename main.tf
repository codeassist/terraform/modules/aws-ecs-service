locals {
  enabled = var.ecs_service_module_enabled ? true : false

  # custom tags are currently not supported until `opt-in` will be enabled (after Jan, 2020)
  tags = {
    terraform = true,
  }
}


# ---------------------------------------------------------------------------
# Setup required IAM resources (policies and role) to be used by ECS Service
# ---------------------------------------------------------------------------
data "aws_iam_policy_document" "ecs_service_assume_role" {
  # Generates an IAM policy document in JSON format.
  # This is a data source which can be used to construct a JSON representation of an IAM policy document, for use with
  # resources which expect policy documents, such as the aws_iam_policy resource.
  count = local.enabled ? 1 : 0

  statement {
    sid     = "AllowECSServiceAssumeTheRole"
    effect  = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["ecs.amazonaws.com"]
    }
  }
}
resource "aws_iam_role" "ecs_service" {
  # Provides an IAM role.

  # NOTE: If policies are attached to the role via the `aws_iam_policy_attachment` resource and you are modifying the
  # role `name` or `path`, the `force_detach_policies` argument must be set to `true` and applied before attempting the
  # operation otherwise you will encounter a `DeleteConflict` error.
  # The `aws_iam_role_policy_attachment` resource (recommended) does not have this requirement.
  count = local.enabled ? 1 : 0

  # (Optional, Forces new resource) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = "ecs-service-role-"
  # (Optional) The description of the role.
  description = "The IAM role for ECS [${var.ecs_service_name}] Service."
  # Key-value mapping of tags for the IAM role.
  tags = merge(
    local.tags,
    {
      Name = format("%s-ecs-service-role", var.ecs_service_name),
    },
  )

  # (Required) The policy that grants an entity permission to assume the role.
  assume_role_policy = join("", data.aws_iam_policy_document.ecs_service_assume_role.*.json)
}
data "aws_iam_policy_document" "ecs_service" {
  # Generates an IAM policy document in JSON format.
  # This is a data source which can be used to construct a JSON representation of an IAM policy document, for use with
  # resources which expect policy documents, such as the aws_iam_policy resource.
  count = local.enabled ? 1 : 0

  statement {
    sid    = "AllowECSServicePerformSpecifiedOperations"
    effect = "Allow"
    actions = [
      "elasticloadbalancing:Describe*",
      "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
      "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
      "ec2:Describe*",
      "ec2:AuthorizeSecurityGroupIngress"
    ]
    resources = ["*"]
  }
}
resource "aws_iam_role_policy" "ecs_service" {
  # Provides an IAM role policy.
  count = local.enabled ? 1 : 0

  # (Optional) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = "ecs-service-role-policy-"
  # (Required) The policy document. This is a JSON formatted string.
  policy = join("", data.aws_iam_policy_document.ecs_service.*.json)
  # (Required) The IAM role to attach to the policy.
  role = join("", aws_iam_role.ecs_service.*.id)
}


# ---------------------------
# Network/Security resources
# ---------------------------
# NOTE(!): to prevent tf hangs/fails when changing security group resource in-place let's add lifecycle and use
# `name_prefix`, see:
#   * https://github.com/hashicorp/terraform/issues/8617
resource "aws_security_group" "ecs_service" {
  # Provides a security group resource.
  count = local.enabled ? 1 : 0

  # (Optional, Forces new resource) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = "ecs-service-sg-"
  # (Optional, Forces new resource) The security group description. Defaults to "Managed by Terraform".
  description = "Security Group for ECS [${var.ecs_service_name}] Service."
  # (Optional) A mapping of tags to assign to the resource.
  tags = merge(
    local.tags,
    {
      Name = format("%s-ecs-service-sg", var.ecs_service_name),
    },
  )

  # (Optional, Forces new resource) The VPC ID.
  vpc_id = var.ecs_service_vpc_id
}
resource "aws_security_group_rule" "allow_egress_all" {
  # Provides a security group rule resource. Represents a single ingress or egress group rule, which can be added to
  # external Security Groups.
  count = local.enabled ? 1 : 0

  # (Required) The type of rule being created. Valid options are ingress (inbound) or egress (outbound).
  type = "egress"
  # (Optional) Description of the rule.
  description = "Allow ALL Egress from the ECS Service."
  # (Required) The start port (or ICMP type number if protocol is "icmp").
  from_port = 0
  # (Required) The end port (or ICMP code if protocol is "icmp").
  to_port = 0
  # (Required) The protocol. If not `icmp`, `tcp`, `udp`, or `all` use the protocol number, see at:
  #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  protocol = "-1"
  # (Optional) List of CIDR blocks. Cannot be specified with `source_security_group_id`.
  cidr_blocks = ["0.0.0.0/0"]
  # (Required) The security group to apply this rule to.
  security_group_id = join("", aws_security_group.ecs_service.*.id)
}
resource "aws_security_group_rule" "allow_ingress_from_sg" {
  # Provides a security group rule resource. Represents a single ingress or egress group rule, which can be added to
  # external Security Groups.
  count = local.enabled ? var.ecs_service_allowed_security_groups_count : 0

  # (Required) The type of rule being created. Valid options are ingress (inbound) or egress (outbound).
  type = "ingress"
  # (Optional) Description of the rule.
  description = "Allow ingress connections from the specified Security Group."
  # (Required) The start port (or ICMP type number if protocol is "icmp").
  from_port = 0
  # (Required) The end port (or ICMP code if protocol is "icmp").
  to_port = 65535
  # (Required) The protocol. If not `icmp`, `tcp`, `udp`, or `all` use the protocol number, see at:
  #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  protocol = "-1"
  # (Optional) List of CIDR blocks. Cannot be specified with `source_security_group_id`.
  source_security_group_id = var.ecs_service_allowed_security_groups_list[count.index]
  # (Required) The security group to apply this rule to.
  security_group_id = join("", aws_security_group.ecs_service.*.id)
}
resource "aws_security_group_rule" "allow_ingress_cidrs" {
  # Provides a security group rule resource. Represents a single ingress or egress group rule, which can be added to
  # external Security Groups.
  count = (local.enabled && var.ecs_service_allowed_cidr_blocks_count > 0) ? 1 : 0

  # (Required) The type of rule being created. Valid options are ingress (inbound) or egress (outbound).
  type = "ingress"
  # (Optional) Description of the rule.
  description = "Allow ingress connections from the specified CIDR blocks."
  # (Required) The start port (or ICMP type number if protocol is "icmp").
  from_port = 0
  # (Required) The end port (or ICMP code if protocol is "icmp").
  to_port = 65535
  # (Required) The protocol. If not `icmp`, `tcp`, `udp`, or `all` use the protocol number, see at:
  #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  protocol = "-1"
  # (Optional) List of CIDR blocks. Cannot be specified with `source_security_group_id`.
  cidr_blocks = var.ecs_service_allowed_cidr_blocks
  # (Required) The security group to apply this rule to.
  security_group_id = join("", aws_security_group.ecs_service.*.id)
}
resource "aws_security_group_rule" "allow_ingress_ports" {
  # Provides a security group rule resource. Represents a single ingress or egress group rule, which can be added to
  # external Security Groups.
  count = local.enabled ? length(var.ecs_service_open_ports_list) : 0

  # (Required) The type of rule being created. Valid options are ingress (inbound) or egress (outbound).
  type = "ingress"
  # (Optional) Description of the rule.
  description = "Allow ingress traffic to the ECS Service on [${var.ecs_service_open_ports_list[count.index]}] port from ANY IP."
  # (Required) The start port (or ICMP type number if protocol is "icmp").
  from_port = var.ecs_service_open_ports_list[count.index]
  # (Required) The end port (or ICMP code if protocol is "icmp").
  to_port = var.ecs_service_open_ports_list[count.index]
  # (Required) The protocol. If not `icmp`, `tcp`, `udp`, or `all` use the protocol number, see at:
  #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  protocol = "tcp"
  # (Optional) List of CIDR blocks. Cannot be specified with `source_security_group_id`.
  cidr_blocks = ["0.0.0.0/0"]
  # (Required) The security group to apply this rule to.
  security_group_id = join("", aws_security_group.ecs_service.*.id)
}


# -------------------------------------------------------------------------------------------------------
# Setup ECS Service resource with lifecycle depending on `ecs_service_ignore_parameter_changes` variable
# -------------------------------------------------------------------------------------------------------
resource "aws_ecs_service" "default" {
  # Provides an ECS service - effectively a task that is expected to run until an error occurs or a user terminates it
  # (typically a webserver or a database).
  count = (local.enabled && lower(var.ecs_service_ignore_parameter_changes) != "desired_count" && lower(var.ecs_service_ignore_parameter_changes) != "task_definition") ? 1 : 0

  # Note: To prevent a race condition during service deletion, make sure to set depends_on to the related
  # `aws_iam_role_policy`; otherwise, the policy may be destroyed too soon and the ECS service will then get stuck
  # in the DRAINING state.
  depends_on = [
    # See:
    #   * https://discuss.hashicorp.com/t/tips-howto-implement-module-depends-on-emulation/2305
    var.ecs_service_module_depends_on,
    aws_iam_role_policy.ecs_service,
    aws_security_group.ecs_service,
  ]

  # (Required) The name of the service (up to 255 letters, numbers, hyphens, and underscores).
  name = var.ecs_service_name
  # (Required) The family and revision (family:revision) or full ARN of the task definition that you want to run in
  # your service.
  task_definition = var.ecs_service_task_definition_arn

  # (Optional) The capacity provider strategy to use for the service. Can be one or more.
  dynamic "capacity_provider_strategy" {
    for_each = var.ecs_service_capacity_provider_strategy
    content {
      # (Required) The short name or full Amazon Resource Name (ARN) of the capacity provider.
      capacity_provider = lookup(capacity_provider_strategy.value, "capacity_provider", "")
      # (Required) The relative percentage of the total number of launched tasks that should use the specified capacity
      # provider.
      weight = lookup(capacity_provider_strategy.value, "weight", 0)
      # (Optional) The number of tasks, at a minimum, to run on the specified capacity provider. Only one capacity
      # provider in a capacity provider strategy can have a base defined.
      base = lookup(capacity_provider_strategy.value, "base", null)
    }
  }

  # (Optional) ARN of an ECS cluster.
  cluster = var.ecs_service_cluster_arn

  # (Optional) Configuration block containing deployment controller configuration.
  deployment_controller {
    # (Optional) Type of deployment controller. Valid values:
    #   * CODE_DEPLOY
    #   * ECS.
    type = var.ecs_service_deployment_controller_type
  }

  # (Optional) The lower limit (as a percentage of the service's desiredCount) of the number of running tasks that must
  # remain running and healthy in a service during a deployment.
  deployment_minimum_healthy_percent = var.ecs_service_deployment_minimum_healthy_percent
  # (Optional) The upper limit (as a percentage of the service's desiredCount) of the number of running tasks that can
  # be running in a service during a deployment. Not valid when using the `DAEMON` scheduling strategy.
  deployment_maximum_percent = var.ecs_service_deployment_maximum_percent
  # (Optional) The number of instances of the task definition to place and keep running. Defaults to 0. Do not specify
  # if using the `DAEMON` scheduling strategy.
  desired_count = var.ecs_service_desired_count
  # (Optional) Seconds to ignore failing load balancer health checks on newly instantiated tasks to prevent premature
  # shutdown, up to 2147483647. Only valid for services configured to use load balancers.
  health_check_grace_period_seconds = var.ecs_service_health_check_grace_period_seconds
  # (Optional) The launch type on which to run your service. The valid values are EC2 and FARGATE. Defaults to EC2.
  launch_type = var.ecs_service_launch_type

  # (Optional) A load balancer block.
  dynamic "load_balancer" {
    for_each = var.ecs_service_load_balancers
    content {
      # (Required for ELB Classic) The name of the ELB (Classic) to associate with the service.
      elb_name = lookup(load_balancer.value, "elb_name", null)
      # (Required for ALB/NLB) The ARN of the Load Balancer target group to associate with the service.
      target_group_arn = lookup(load_balancer.value, "target_group_arn", null)
      # (Required) The name of the container to associate with the load balancer (as it appears in a container
      # definition).
      container_name = load_balancer.value.container_name
      #  (Required) The port on the container to associate with the load balancer.
      container_port = load_balancer.value.container_port
    }
  }

  # (Optional) The network configuration for the service. This parameter is required for task definitions that use the
  # `awsvpc` network mode to receive their own Elastic Network Interface, and it is not supported for other network
  # modes.
  dynamic "network_configuration" {
    for_each = var.ecs_service_task_network_mode == "awsvpc" ? ["true"] : []
    content {
      # (Required) The subnets associated with the task or service.
      subnets = var.ecs_service_subnet_ids
      #  (Optional) The security groups associated with the task or service. If you do not specify a security group,
      # the default security group for the VPC is used.
      security_groups = compact(concat(var.ecs_service_security_group_ids, aws_security_group.ecs_service.*.id))
      # (Optional) Assign a public IP address to the ENI (FARGATE launch type only). Valid values are:
      #   * true
      #   * false.
      # Default false.
      assign_public_ip = var.ecs_service_assign_public_ip
    }
  }

  # (Optional) Service level strategy rules that are taken into consideration during task placement.
  # List from top to bottom in order of precedence.
  # The maximum number of `ordered_placement_strategy` blocks is 5.
  dynamic "ordered_placement_strategy" {
    for_each = var.ecs_service_ordered_placement_strategy
    content {
      # (Required) The type of placement strategy. Must be one of:
      #   * binpack
      #   * random
      #   * spread
      type = lookup(ordered_placement_strategy.value, "type", "")
      # (Optional) For the spread placement strategy, valid values are "instanceId" (or "host", which has the same
      # effect), or any platform or custom attribute that is applied to a container instance.
      # For the "binpack" type, valid values are "memory" and "cpu".
      # For the "random" type, this attribute is not needed.
      # For more information, see Placement Strategy:
      #   * https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_PlacementStrategy.html
      field = lookup(ordered_placement_strategy.value, "field", null)
    }
  }

  # (Optional) rules that are taken into consideration during task placement. Maximum number of
  # `placement_constraints` is 10.
  dynamic "placement_constraints" {
    for_each = var.ecs_service_placement_constraints
    content {
      # (Required) The type of constraint. The only valid values at this time are "memberOf" and "distinctInstance".
      type = lookup(placement_constraints.value, "type", "")
      # (Optional) Cluster Query Language expression to apply to the constraint. Does not need to be specified for
      # the "distinctInstance" type. For more information, see:
      #   * https://docs.aws.amazon.com/AmazonECS/latest/developerguide/cluster-query-language.html
      expression = lookup(placement_constraints.value, "expression", null)
    }
  }

  # (Optional) Specifies whether to propagate the tags from the task definition or the service to the tasks. The valid
  # values are:
  #   * SERVICE
  #   * TASK_DEFINITION.
  propagate_tags = var.ecs_service_propagate_tags

  # (Optional) The service discovery registries for the service. The maximum number of service_registries blocks is 1.
  dynamic "service_registries" {
    for_each = var.ecs_service_discovery_service_registries
    content {
      # (Required) The ARN of the Service Registry. The currently supported service registry is Amazon Route 53 Auto
      # Naming Service(`aws_service_discovery_service`). For more information, see:
      #   * https://docs.aws.amazon.com/Route53/latest/APIReference/API_autonaming_Service.html
      registry_arn = service_registries.value.registry_arn
      # (Optional) The port value used if your Service Discovery service specified an SRV record.
      port = lookup(service_registries.value, "port", null)
      # (Optional) The port value, already specified in the task definition, to be used for your service discovery
      # service.
      container_port = lookup(service_registries.value, "container_port", null)
      # (Optional) The container name value, already specified in the task definition, to be used for your service
      # discovery service.
      container_name = lookup(service_registries.value, "container_name", null)
    }
  }
}


resource "aws_ecs_service" "ignored_desired_count" {
  # Provides an ECS service - effectively a task that is expected to run until an error occurs or a user terminates it
  # (typically a webserver or a database).
  count = (local.enabled && lower(var.ecs_service_ignore_parameter_changes) == "desired_count") ? 1 : 0

  # Note: To prevent a race condition during service deletion, make sure to set depends_on to the related
  # `aws_iam_role_policy`; otherwise, the policy may be destroyed too soon and the ECS service will then get stuck
  # in the DRAINING state.
  depends_on = [
    # See:
    #   * https://discuss.hashicorp.com/t/tips-howto-implement-module-depends-on-emulation/2305
    var.ecs_service_module_depends_on,
    aws_iam_role_policy.ecs_service,
    aws_security_group.ecs_service,
  ]

  # You can utilize the generic Terraform resource lifecycle configuration block with ignore_changes to create an ECS
  # service with an initial count of running instances, then ignore any changes to that count caused externally
  # (e.g. Application Autoscaling).
  lifecycle {
    # Allow specified external changes without Terraform plan difference.
    ignore_changes = [
      desired_count
    ]
  }

  # (Required) The name of the service (up to 255 letters, numbers, hyphens, and underscores).
  name = var.ecs_service_name
  # (Required) The family and revision (family:revision) or full ARN of the task definition that you want to run in
  # your service.
  task_definition = var.ecs_service_task_definition_arn

  # (Optional) The capacity provider strategy to use for the service. Can be one or more.
  dynamic "capacity_provider_strategy" {
    for_each = var.ecs_service_capacity_provider_strategy
    content {
      # (Required) The short name or full Amazon Resource Name (ARN) of the capacity provider.
      capacity_provider = lookup(capacity_provider_strategy.value, "capacity_provider", "")
      # (Required) The relative percentage of the total number of launched tasks that should use the specified capacity
      # provider.
      weight = lookup(capacity_provider_strategy.value, "weight", 0)
      # (Optional) The number of tasks, at a minimum, to run on the specified capacity provider. Only one capacity
      # provider in a capacity provider strategy can have a base defined.
      base = lookup(capacity_provider_strategy.value, "base", null)
    }
  }

  # (Optional) ARN of an ECS cluster.
  cluster = var.ecs_service_cluster_arn

  # (Optional) Configuration block containing deployment controller configuration.
  deployment_controller {
    # (Optional) Type of deployment controller. Valid values:
    #   * CODE_DEPLOY
    #   * ECS.
    type = var.ecs_service_deployment_controller_type
  }

  # (Optional) The lower limit (as a percentage of the service's desiredCount) of the number of running tasks that must
  # remain running and healthy in a service during a deployment.
  deployment_minimum_healthy_percent = var.ecs_service_deployment_minimum_healthy_percent
  # (Optional) The upper limit (as a percentage of the service's desiredCount) of the number of running tasks that can
  # be running in a service during a deployment. Not valid when using the `DAEMON` scheduling strategy.
  deployment_maximum_percent = var.ecs_service_deployment_maximum_percent
  # (Optional) The number of instances of the task definition to place and keep running. Defaults to 0. Do not specify
  # if using the `DAEMON` scheduling strategy.
  desired_count = var.ecs_service_desired_count
  # (Optional) Seconds to ignore failing load balancer health checks on newly instantiated tasks to prevent premature
  # shutdown, up to 2147483647. Only valid for services configured to use load balancers.
  health_check_grace_period_seconds = var.ecs_service_health_check_grace_period_seconds
  # (Optional) The launch type on which to run your service. The valid values are EC2 and FARGATE. Defaults to EC2.
  launch_type = var.ecs_service_launch_type

  # (Optional) A load balancer block.
  dynamic "load_balancer" {
    for_each = var.ecs_service_load_balancers
    content {
      # (Required for ELB Classic) The name of the ELB (Classic) to associate with the service.
      elb_name = lookup(load_balancer.value, "elb_name", null)
      # (Required for ALB/NLB) The ARN of the Load Balancer target group to associate with the service.
      target_group_arn = lookup(load_balancer.value, "target_group_arn", null)
      # (Required) The name of the container to associate with the load balancer (as it appears in a container
      # definition).
      container_name = load_balancer.value.container_name
      #  (Required) The port on the container to associate with the load balancer.
      container_port = load_balancer.value.container_port
    }
  }

  # (Optional) The network configuration for the service. This parameter is required for task definitions that use the
  # `awsvpc` network mode to receive their own Elastic Network Interface, and it is not supported for other network
  # modes.
  dynamic "network_configuration" {
    for_each = var.ecs_service_task_network_mode == "awsvpc" ? ["true"] : []
    content {
      # (Required) The subnets associated with the task or service.
      subnets = var.ecs_service_subnet_ids
      #  (Optional) The security groups associated with the task or service. If you do not specify a security group,
      # the default security group for the VPC is used.
      security_groups = compact(concat(var.ecs_service_security_group_ids, aws_security_group.ecs_service.*.id))
      # (Optional) Assign a public IP address to the ENI (FARGATE launch type only). Valid values are:
      #   * true
      #   * false.
      # Default false.
      assign_public_ip = var.ecs_service_assign_public_ip
    }
  }

  # (Optional) Service level strategy rules that are taken into consideration during task placement.
  # List from top to bottom in order of precedence.
  # The maximum number of `ordered_placement_strategy` blocks is 5.
  dynamic "ordered_placement_strategy" {
    for_each = var.ecs_service_ordered_placement_strategy
    content {
      # (Required) The type of placement strategy. Must be one of:
      #   * binpack
      #   * random
      #   * spread
      type = lookup(ordered_placement_strategy.value, "type", "")
      # (Optional) For the spread placement strategy, valid values are "instanceId" (or "host", which has the same
      # effect), or any platform or custom attribute that is applied to a container instance.
      # For the "binpack" type, valid values are "memory" and "cpu".
      # For the "random" type, this attribute is not needed.
      # For more information, see Placement Strategy:
      #   * https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_PlacementStrategy.html
      field = lookup(ordered_placement_strategy.value, "field", null)
    }
  }

  # (Optional) rules that are taken into consideration during task placement. Maximum number of
  # `placement_constraints` is 10.
  dynamic "placement_constraints" {
    for_each = var.ecs_service_placement_constraints
    content {
      # (Required) The type of constraint. The only valid values at this time are "memberOf" and "distinctInstance".
      type = lookup(placement_constraints.value, "type", "")
      # (Optional) Cluster Query Language expression to apply to the constraint. Does not need to be specified for
      # the "distinctInstance" type. For more information, see:
      #   * https://docs.aws.amazon.com/AmazonECS/latest/developerguide/cluster-query-language.html
      expression = lookup(placement_constraints.value, "expression", null)
    }
  }

  # (Optional) Specifies whether to propagate the tags from the task definition or the service to the tasks. The valid
  # values are:
  #   * SERVICE
  #   * TASK_DEFINITION.
  propagate_tags = var.ecs_service_propagate_tags

  # (Optional) The service discovery registries for the service. The maximum number of service_registries blocks is 1.
  dynamic "service_registries" {
    for_each = var.ecs_service_discovery_service_registries
    content {
      # (Required) The ARN of the Service Registry. The currently supported service registry is Amazon Route 53 Auto
      # Naming Service(`aws_service_discovery_service`). For more information, see:
      #   * https://docs.aws.amazon.com/Route53/latest/APIReference/API_autonaming_Service.html
      registry_arn = service_registries.value.registry_arn
      # (Optional) The port value used if your Service Discovery service specified an SRV record.
      port = lookup(service_registries.value, "port", null)
      # (Optional) The port value, already specified in the task definition, to be used for your service discovery
      # service.
      container_port = lookup(service_registries.value, "container_port", null)
      # (Optional) The container name value, already specified in the task definition, to be used for your service
      # discovery service.
      container_name = lookup(service_registries.value, "container_name", null)
    }
  }
}


resource "aws_ecs_service" "ignored_task_definition" {
  # Provides an ECS service - effectively a task that is expected to run until an error occurs or a user terminates it
  # (typically a webserver or a database).
  count = (local.enabled && lower(var.ecs_service_ignore_parameter_changes) == "task_definition") ? 1 : 0

  # Note: To prevent a race condition during service deletion, make sure to set depends_on to the related
  # `aws_iam_role_policy`; otherwise, the policy may be destroyed too soon and the ECS service will then get stuck
  # in the DRAINING state.
  depends_on = [
    # See:
    #   * https://discuss.hashicorp.com/t/tips-howto-implement-module-depends-on-emulation/2305
    var.ecs_service_module_depends_on,
    aws_iam_role_policy.ecs_service,
    aws_security_group.ecs_service,
  ]

  # You can utilize the generic Terraform resource lifecycle configuration block with ignore_changes to create an ECS
  # service with an initial count of running instances, then ignore any changes to that count caused externally
  # (e.g. Application Autoscaling).
  lifecycle {
    # Allow specified external changes without Terraform plan difference.
    ignore_changes = [
      task_definition
    ]
  }

  # (Required) The name of the service (up to 255 letters, numbers, hyphens, and underscores).
  name = var.ecs_service_name
  # (Required) The family and revision (family:revision) or full ARN of the task definition that you want to run in
  # your service.
  task_definition = var.ecs_service_task_definition_arn

  # (Optional) The capacity provider strategy to use for the service. Can be one or more.
  dynamic "capacity_provider_strategy" {
    for_each = var.ecs_service_capacity_provider_strategy
    content {
      # (Required) The short name or full Amazon Resource Name (ARN) of the capacity provider.
      capacity_provider = lookup(capacity_provider_strategy.value, "capacity_provider", "")
      # (Required) The relative percentage of the total number of launched tasks that should use the specified capacity
      # provider.
      weight = lookup(capacity_provider_strategy.value, "weight", 0)
      # (Optional) The number of tasks, at a minimum, to run on the specified capacity provider. Only one capacity
      # provider in a capacity provider strategy can have a base defined.
      base = lookup(capacity_provider_strategy.value, "base", null)
    }
  }

  # (Optional) ARN of an ECS cluster.
  cluster = var.ecs_service_cluster_arn

  # (Optional) Configuration block containing deployment controller configuration.
  deployment_controller {
    # (Optional) Type of deployment controller. Valid values:
    #   * CODE_DEPLOY
    #   * ECS.
    type = var.ecs_service_deployment_controller_type
  }

  # (Optional) The lower limit (as a percentage of the service's desiredCount) of the number of running tasks that must
  # remain running and healthy in a service during a deployment.
  deployment_minimum_healthy_percent = var.ecs_service_deployment_minimum_healthy_percent
  # (Optional) The upper limit (as a percentage of the service's desiredCount) of the number of running tasks that can
  # be running in a service during a deployment. Not valid when using the `DAEMON` scheduling strategy.
  deployment_maximum_percent = var.ecs_service_deployment_maximum_percent
  # (Optional) The number of instances of the task definition to place and keep running. Defaults to 0. Do not specify
  # if using the `DAEMON` scheduling strategy.
  desired_count = var.ecs_service_desired_count
  # (Optional) Seconds to ignore failing load balancer health checks on newly instantiated tasks to prevent premature
  # shutdown, up to 2147483647. Only valid for services configured to use load balancers.
  health_check_grace_period_seconds = var.ecs_service_health_check_grace_period_seconds
  # (Optional) The launch type on which to run your service. The valid values are EC2 and FARGATE. Defaults to EC2.
  launch_type = var.ecs_service_launch_type

  # (Optional) A load balancer block.
  dynamic "load_balancer" {
    for_each = var.ecs_service_load_balancers
    content {
      # (Required for ELB Classic) The name of the ELB (Classic) to associate with the service.
      elb_name = lookup(load_balancer.value, "elb_name", null)
      # (Required for ALB/NLB) The ARN of the Load Balancer target group to associate with the service.
      target_group_arn = lookup(load_balancer.value, "target_group_arn", null)
      # (Required) The name of the container to associate with the load balancer (as it appears in a container
      # definition).
      container_name = load_balancer.value.container_name
      #  (Required) The port on the container to associate with the load balancer.
      container_port = load_balancer.value.container_port
    }
  }

  # (Optional) The network configuration for the service. This parameter is required for task definitions that use the
  # `awsvpc` network mode to receive their own Elastic Network Interface, and it is not supported for other network
  # modes.
  dynamic "network_configuration" {
    for_each = var.ecs_service_task_network_mode == "awsvpc" ? ["true"] : []
    content {
      # (Required) The subnets associated with the task or service.
      subnets = var.ecs_service_subnet_ids
      #  (Optional) The security groups associated with the task or service. If you do not specify a security group,
      # the default security group for the VPC is used.
      security_groups = compact(concat(var.ecs_service_security_group_ids, aws_security_group.ecs_service.*.id))
      # (Optional) Assign a public IP address to the ENI (FARGATE launch type only). Valid values are:
      #   * true
      #   * false.
      # Default false.
      assign_public_ip = var.ecs_service_assign_public_ip
    }
  }

  # (Optional) Service level strategy rules that are taken into consideration during task placement.
  # List from top to bottom in order of precedence.
  # The maximum number of `ordered_placement_strategy` blocks is 5.
  dynamic "ordered_placement_strategy" {
    for_each = var.ecs_service_ordered_placement_strategy
    content {
      # (Required) The type of placement strategy. Must be one of:
      #   * binpack
      #   * random
      #   * spread
      type = lookup(ordered_placement_strategy.value, "type", "")
      # (Optional) For the spread placement strategy, valid values are "instanceId" (or "host", which has the same
      # effect), or any platform or custom attribute that is applied to a container instance.
      # For the "binpack" type, valid values are "memory" and "cpu".
      # For the "random" type, this attribute is not needed.
      # For more information, see Placement Strategy:
      #   * https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_PlacementStrategy.html
      field = lookup(ordered_placement_strategy.value, "field", null)
    }
  }

  # (Optional) rules that are taken into consideration during task placement. Maximum number of
  # `placement_constraints` is 10.
  dynamic "placement_constraints" {
    for_each = var.ecs_service_placement_constraints
    content {
      # (Required) The type of constraint. The only valid values at this time are "memberOf" and "distinctInstance".
      type = lookup(placement_constraints.value, "type", "")
      # (Optional) Cluster Query Language expression to apply to the constraint. Does not need to be specified for
      # the "distinctInstance" type. For more information, see:
      #   * https://docs.aws.amazon.com/AmazonECS/latest/developerguide/cluster-query-language.html
      expression = lookup(placement_constraints.value, "expression", null)
    }
  }

  # (Optional) Specifies whether to propagate the tags from the task definition or the service to the tasks. The valid
  # values are:
  #   * SERVICE
  #   * TASK_DEFINITION.
  propagate_tags = var.ecs_service_propagate_tags

  # (Optional) The service discovery registries for the service. The maximum number of service_registries blocks is 1.
  dynamic "service_registries" {
    for_each = var.ecs_service_discovery_service_registries
    content {
      # (Required) The ARN of the Service Registry. The currently supported service registry is Amazon Route 53 Auto
      # Naming Service(`aws_service_discovery_service`). For more information, see:
      #   * https://docs.aws.amazon.com/Route53/latest/APIReference/API_autonaming_Service.html
      registry_arn = service_registries.value.registry_arn
      # (Optional) The port value used if your Service Discovery service specified an SRV record.
      port = lookup(service_registries.value, "port", null)
      # (Optional) The port value, already specified in the task definition, to be used for your service discovery
      # service.
      container_port = lookup(service_registries.value, "container_port", null)
      # (Optional) The container name value, already specified in the task definition, to be used for your service
      # discovery service.
      container_name = lookup(service_registries.value, "container_name", null)
    }
  }
}
